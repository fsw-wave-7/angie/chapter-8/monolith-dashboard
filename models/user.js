'use strict'
const {
  Model
} = require('sequelize');
const bcrypt = require('bcrypt')
const admin = {
  id: Math.floor(Math.random() * 100),
  username: 'admin',
  password: bcrypt.hashSync('123456', 10)
}

module.exports = (sequelize, DataTypes) => {
  class User extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  User.init({
    username: DataTypes.STRING,
    password: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'Visitor',
  });

  User.prototype.verifyPassword = function (password) {
    return bcrypt.compareSync(password, this.password)
  }

  User.findOne = function ({
    username
  }) {
    if (username !== 'admin') return Promise.resolve(null)
    const user = new User(admin)
    return Promise.resolve(user)
  }

  User.finfByPk = function (id) {
    if (admin.id === id) {
      const user = new User(admin)
      return Promise.resolve(user)
    }
    return Promise.resolve(null)
  }
  return User;
  };


