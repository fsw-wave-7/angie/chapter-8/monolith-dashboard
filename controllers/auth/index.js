module.exports = {
    login: (req, res) => res.render('pages/authentication/login',
        {
            layout: 'layout/dashboard',
            contentName:  'Login'   }
),
    register: (req, res) => res.render('pages/authentication/register'),
    api: require('./api')
}