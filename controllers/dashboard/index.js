const { Post, Visitor } = require('../../models')

module.exports = {
    home: async (req, res) => {
        const locals = {
            data: [{ Post: 10, Visitor: 100, Reader: 10 }],
            contentName: 'Statistic',
            layout: 'layout/dashboard'
        }
        console.log(locals)
        res.render('pages/dashboard/home',locals)
    },
    post: require('./post')
}