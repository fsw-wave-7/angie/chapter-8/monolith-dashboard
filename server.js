const express = require('express')
const app = express()
const { PORT = 8000 } = process.env
const expressLayout = require('express-ejs-layouts')
const morgan = require('morgan')
const setDefault = (req, res, next) => {
    res.locals.contentName = "Default"
    next()
}
const router = require("./router")

app.use(express.static('public'))
app.use(expressLayout)
app.use(express.urlencoded({ extended: false }))
app.use(express.json())

app.set('layout', 'layout/default')
app.set('view engine', 'ejs')
app.use(morgan('dev'))

app.use(router)


app.listen(PORT, () => {
    console.log(`Listening on http://localhost:${PORT}`)
})