const router = require('express').Router()
const { dashboard } = require('../controllers')
const post = require('../controllers/dashboard/post')
// const layoutName = (req, res, next) => {
//     // res.locals.layout = `layouts/${layoutName}`
//     next()
// }
const authenticate = (req, res, next) => {
    if (req.isAuthenticated()) { return next() }
    res.redirect('/login')
}

// router.use(layoutName('dashboard'))
router.use(authenticate)
router.get('/', dashboard.home)
router.get('/post', dashboard.post.index)

module.exports = router